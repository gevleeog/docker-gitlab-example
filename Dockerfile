FROM microsoft/dotnet

RUN mkdir /app

COPY ./src/DockerGitlabExampleApi/OUT/ ./app

EXPOSE 5000

CMD ["dotnet","/app/DockerGitlabExampleApi.dll"]